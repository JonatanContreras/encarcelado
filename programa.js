class Encarcelado{

    //Atributos 
    _palabra;
    _letra;


    //Metodo constructor se ejecuta cuando se crea un objeto 
    constructor(valor){
        this._palabra=valor;
    }

    set palabra(valor){
        this._palabra=valor;
    }

    set letra(valor) {
        this._letra = valor;
    }


    inciarJuego(){

    //Verificar el tamaño de la palabra.
        let a=this._palabra.length;
        let boton;
        let formulario=document.getElementById("frmTablero"); 


        for(let i=0;i<a;i++){

            boton=document.createElement("input");
            boton.setAttribute("type","button");
            boton.setAttribute("class", "boton");
            boton.setAttribute("id", "boton" + i);

            formulario.appendChild(boton);

        }
    }



    //Verificar si la letra pertenece a la palabra
    verificarLetra(){
        let j = this._palabra.length;
        let u = ["u", "n", "i", "v", "e", "r", "s", "i", "d", "a", "d"];
        let i = 0;
        while (i < j) {
            if (this._letra == u[i]) {
                alert("Está letra si pertenece a la palabra.");
                return this._letra;
            } 
            else {
                alert("Está letra no pertenece a la palabra.");
                return this._letra;
            }
        }
    }
}

    
let miJuego=new Encarcelado("universidad");
miJuego.inciarJuego();